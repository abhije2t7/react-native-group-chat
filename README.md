# react-native-group-chat

A simple group chat application build with React Native and Firebase Realtime Database

## Getting started
Clone the repo in your local machine

# installation

$ cd react-native-group-chat
$ npm install

Install the dependencies and devDependencies and start the server.

Create Firebase project and services files to respective folders

Run the project