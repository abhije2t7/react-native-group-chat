import * as sc from 'styled-components';


export const { default: styled, css, withTheme, ThemeProvider } = sc;

