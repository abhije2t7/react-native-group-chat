import { styled } from "./Theme";
import { View } from "react-native";


export const Container = styled(View)`
    flex: 1;
    background-color: #fff;
`;
