import { NavigationContainer } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { useAuth } from './AuthProvider';
import AuthStack from './AuthStack';
import auth from '@react-native-firebase/auth';
import AppStack from './AppStack';

const Routes = () => {
    const { user, setUser } = useAuth();
    const [initializing, setInitializing] = useState(true);

    useEffect(() => {
        const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
        return subscriber;
    }, []);

    const onAuthStateChanged = (user: any) => {
        setUser(user);
        if (initializing) setInitializing(false);
    }

    if (initializing) return null;


    return (
        <NavigationContainer>
            {user ? <AppStack /> : <AuthStack />}
        </NavigationContainer>
    );
}

export default Routes;