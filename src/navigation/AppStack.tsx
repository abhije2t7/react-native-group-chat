import React, { useEffect, useState } from "react";
import { createStackNavigator } from '@react-navigation/stack';
import Home from "../screens/Home";
import CreateGroup from "../screens/CreateGroup";
import Settings from "../screens/Settings";
import ChatScreen from "../screens/ChatScreen";

const Stack = createStackNavigator();

const AppStack = () => {

    return (
        <Stack.Navigator initialRouteName={"Home"}>
            <Stack.Screen
                name="Home"
                component={Home}
                options={{ header: () => null }}
            />
            <Stack.Screen
                name="CreateGroup"
                component={CreateGroup}
                options={{ header: () => null }}
            />
            <Stack.Screen
                name="Settings"
                component={Settings}
                options={{ header: () => null }}
            />
            <Stack.Screen
                name="ChatScreen"
                component={ChatScreen}
                options={{ header: () => null }}
            />
        </Stack.Navigator>
    )

}

export default AppStack;