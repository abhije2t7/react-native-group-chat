import React, { useEffect, useState } from "react";
import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Onboarding from "../screens/Onboarding";
import Login from "../screens/Login";
import Signup from "../screens/Signup";

const Stack = createStackNavigator();

const AuthStack = () => {
    const [isFirstLaunch, setIsFirstLaunch] = useState<any>(null);
    let routeName;

    useEffect(() => {
        AsyncStorage.getItem('AlreadyLaunched').then((val) => {
            if (val == null) {
                AsyncStorage.setItem('AlreadyLaunched', 'true');
                setIsFirstLaunch(true);
            } else {
                setIsFirstLaunch(false);
            }
        });
    }, []);

    if (isFirstLaunch == null) {
        return null;
    } else if (isFirstLaunch == true) {
        routeName = 'Onboarding';
    } else {
        routeName = 'Login';
    }

    return (
        <Stack.Navigator initialRouteName={routeName}>
            <Stack.Screen
                name="Onboarding"
                component={Onboarding}
                options={{ header: () => null }}
            />
            <Stack.Screen
                name="Login"
                component={Login}
                options={{ header: () => null }}
            />
            <Stack.Screen
                name="Signup"
                component={Signup}
                options={{ header: () => null }}
            />
        </Stack.Navigator>
    )

}

export default AuthStack;