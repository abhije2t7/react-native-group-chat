import React, { createContext, useState, useContext } from 'react';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import database from '@react-native-firebase/database';

export interface IUseAuth {
    user: any | null;
    setUser(value: any): any;
    login(email: any, password: any): any;
    register(details: any): any;
    logout(): any;
    loginError: any | null;
    setLoginError(value: any): any;
    messageLiked: any | null;
    setMessageLiked(value: any): any;
}

const AuthContext = createContext({});

export const useAuth = () => useContext<IUseAuth>(AuthContext as any);

export const AuthProvider = ({ children }: any) => {
    const [user, setUser] = useState({});
    const [loginError, setLoginError] = useState(false);
    const [messageLiked, setMessageLiked] = useState(false);
    return (
        <AuthContext.Provider value={{
            user,
            setUser,
            login: async (email: any, password: any) => {
                try {
                    await auth().signInWithEmailAndPassword(email, password)
                } catch (e) {
                    console.log('Error in Firebase login: ', e);
                    setLoginError(true);
                }
            },
            register: async (details: any) => {
                try {
                    await auth().createUserWithEmailAndPassword(details.email, details.password).then(() => {
                        let userData = {
                            userId: auth().currentUser?.uid,
                            firstName: details.firstName,
                            lastName: details.lastName,
                            email: details.email,
                        }
                        setUser(userData);
                        database().ref(`/users/${auth().currentUser?.uid}`)
                            .set(userData)
                            .then(() => console.log('User registered successfully'))
                    })
                } catch (e) {
                    console.log('Error in Firebase register: ', e);
                }
            },
            logout: async () => {
                try {
                    await auth().signOut();
                } catch (e) {
                    console.log('Error in Firebase register: ', e);
                }
            },
            loginError,
            setLoginError,
            messageLiked,
            setMessageLiked,
        }}>
            {children}
        </AuthContext.Provider>
    )
}