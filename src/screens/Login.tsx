import React, { useState } from 'react';
import { Text, View, ScrollView, StyleSheet, Image, TouchableOpacity } from 'react-native';
import FormButton from '../components/FormButton';
import FormInput from '../components/FormInput';
import { useAuth } from '../navigation/AuthProvider';
import Spacer from '../components/Spacer';
import { colorTheme } from '../utils/ColorTheme';

const Login = ({ navigation }: any) => {
    const { login, setLoginError, loginError } = useAuth();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();

    const loginTapped = () => {
        setLoginError(false);
        login(email, password);
    }


    return (
        <View style={styles.mainContainer}>
            <ScrollView contentContainerStyle={styles.container}>
                <Image
                    source={require('../../assets/logo.png')}
                    style={styles.logo}
                />
                <Text style={styles.text}>Group Chat App</Text>

                <Spacer height={50} />

                <FormInput
                    labelValue={email}
                    onChangeText={(text: any) => setEmail(text)}
                    placeholder="Email"
                    iconType="mail"
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                />

                <FormInput
                    labelValue={password}
                    onChangeText={(text: any) => setPassword(text)}
                    placeholder="Password"
                    iconType="lock"
                    secureTextEntry={true}
                />

                {loginError && <Text style={styles.errorText} >Email or password is incorrect.</Text>}

                <Spacer height={50} />

                <FormButton
                    buttonTitle="Sign In"
                    onPress={() => loginTapped()}
                />

                <TouchableOpacity
                    style={styles.signup}
                    onPress={() => navigation.navigate('Signup')}>
                    <Text style={styles.navButtonText}>
                        Don't have an acount? Click here
                    </Text>
                </TouchableOpacity>

            </ScrollView>
        </View>
    );
}

export default Login;

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colorTheme.white,
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
        backgroundColor: '#FFF',
        flex: 1
    },
    logo: {
        height: 150,
        width: 150,
        resizeMode: 'cover',
    },
    text: {
        fontSize: 28,
        marginBottom: 10,
        color: colorTheme.appThemeTealColor,
    },
    signup: {
        marginVertical: 35,
    },
    navButtonText: {
        fontSize: 18,
        fontWeight: '500',
        color: colorTheme.appThemeTealColor,
    },
    errorText: {
        fontSize: 15,
        color: colorTheme.errorColor
    }
});