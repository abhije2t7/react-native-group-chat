import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, ImageBackground, TextInput, TouchableOpacity, FlatList } from 'react-native';
import moment from 'moment';
import Message from '../components/Message';
import { colorTheme } from '../utils/ColorTheme';
import IconButton from '../components/IconButton';
import AppHeader from '../components/AppHeader';
import auth from '@react-native-firebase/auth';
import database from '@react-native-firebase/database';
import ModalComponent from '../components/Modal';
import { WindowHeight } from '../utils/Dimensions';

const ChatScreen = ({ navigation, route }: any) => {

    const { groupName, groupId } = route.params;
    // const { messageLiked } = useAuth();
    const [msg, setMsg] = useState('');
    const [disabled, setDisabled] = useState(false);
    const [allUserMessages, setallUserMessages] = useState<any>([]);
    const [deleteGroupModal, setDeleteGroupModal] = useState(false);

    useEffect(() => {
        const onChildAdd = database()
            .ref(`/messages/${groupId}`)
            .on('child_added', snapshot => {
                setallUserMessages((prev: any) => [snapshot.val(), ...prev])
            });

        // Stop listening for updates when no longer required
        return () => database().ref(`/messages/${groupId}`).off('child_added', onChildAdd);
    }, [groupId]);

    // useEffect(() => {
    //     const onValueChange = database()
    //         .ref(`/messages/${groupId}`)
    //         .on('value', snapshot => {
    //             // console.log('User data: ', snapshot.val());
    //             setallUserMessages(Object.values(snapshot.val()))
    //         });

    //     // Stop listening for updates when no longer required
    //     return () => database().ref(`/messages/${groupId}`).off('value', onValueChange);
    // }, [messageLiked]);

    const removeSpaces = (text: string) => text && text.replace(/\s/g, '').length;

    const sendMessage = () => {
        setDisabled(true);
        if (msg === '' || removeSpaces(msg) == 0) {
            setMsg('');
            setDisabled(false);
        } else {
            let messageData = {
                message: msg.trim(),
                from: auth().currentUser?.uid,
                groupId,
                sentTime: moment().format(),
                messageType: 'text',
                isLiked: false
            };

            const ref = database()
                .ref(`/messages/${groupId}`)
                .push();
            Object.assign(messageData, { id: ref.key });
            ref.set(messageData).then(() => {
                setMsg('');
                setDisabled(false);
            });
        }
    }

    const deleteGroupTapped = async () => {
        navigation.navigate("Home");
        await database().ref(`/groupList/${groupId}`).remove();
        await database().ref(`/messages/${groupId}`).remove();
    }

    const deleteModal = () => {
        return (
            <ModalComponent
                testID="deleteGroupModal"
                modalStyle={styles.modal}
                isVisible={deleteGroupModal}
                setIsVisible={() => setDeleteGroupModal(false)}

            >
                <View style={styles.mainModalView}>
                    <Text style={styles.modalText}>Do you want to delete group?</Text>
                    <View style={styles.buttonView}>
                        <TouchableOpacity
                            style={styles.modalButtons}
                            onPress={() => deleteGroupTapped()}
                        >
                            <Text style={{
                                color: colorTheme.white
                            }}>Delete</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.modalButtons}
                            onPress={() => setDeleteGroupModal(false)}
                        >
                            <Text style={{
                                color: colorTheme.white
                            }}>Close</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            </ModalComponent>
        )
    }

    const left = (
        <IconButton iconDictionary='antDesign' iconType="left" onPress={() => navigation.navigate("Home")} />
    )

    const right = (
        <IconButton iconDictionary='antDesign' iconType="setting" onPress={() => setDeleteGroupModal(true)} />
    )

    return (
        <View style={styles.container}>
            {deleteModal()}
            <AppHeader
                title={groupName}
                titleColor={colorTheme.white}
                left={left}
                right={right}
                backgroundColor={colorTheme.appThemeTealColor}
            />
            <ImageBackground
                source={require('../../assets/Background.jpg')}
                style={{ flex: 1 }}
            >
                <FlatList
                    style={{ flex: 1 }}
                    data={allUserMessages}
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                    inverted
                    renderItem={({ item }) => {
                        return (
                            <Message
                                sender={item.from == auth().currentUser?.uid}
                                item={item}
                                groupId={groupId}
                            />
                        )
                    }}
                />
            </ImageBackground>

            <View
                style={{
                    backgroundColor: colorTheme.appThemeTealColor,
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingVertical: 7,
                    justifyContent: 'space-evenly',
                }}
            >

                <TextInput
                    style={{
                        backgroundColor: colorTheme.white,
                        width: '80%',
                        borderRadius: 25,
                        borderWidth: 0.5,
                        borderColor: colorTheme.white,
                        paddingHorizontal: 15,
                        color: colorTheme.black,
                    }}
                    placeholder="Type a message"
                    placeholderTextColor={colorTheme.black}
                    multiline={true}
                    value={msg}
                    onChangeText={(val) => setMsg(val)}
                />

                <TouchableOpacity
                    disabled={disabled}
                    onPress={sendMessage}
                    style={{ alignItems: 'center', }}
                >
                    <Text style={{ color: colorTheme.white, fontWeight: 'bold' }}>Send</Text>

                </TouchableOpacity>

            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    modal: {
        justifyContent: 'flex-end',
        margin: 0
    },
    mainModalView: {
        backgroundColor: colorTheme.white,
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        height: WindowHeight / 4,
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalText: {
        color: colorTheme.black,
        fontSize: 20
    },
    buttonView: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        width: '100%',
        paddingTop: 30
    },
    modalButtons: {
        height: 50,
        borderRadius: 10,
        backgroundColor: colorTheme.appThemeTealColor,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        width: '20%',

    },
});

export default ChatScreen;