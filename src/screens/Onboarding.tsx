import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import FormButton from '../components/FormButton';
import Spacer from '../components/Spacer';
import { colorTheme } from '../utils/ColorTheme';

const Onboarding = ({ navigation }: any) => {
    return (
        <View style={styles.container}>
            <Image
                source={require('../../assets/logo.png')}
                style={styles.logo}
            />
            <Text style={styles.text}>Group Chat App</Text>

            <Spacer height={50} />

            <FormButton
                buttonTitle="Get Started"
                onPress={() => navigation.navigate('Login')}
            />
        </View>
    );
}

export default Onboarding;

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,
        paddingTop: 50,
        backgroundColor: '#FFF',
        flex: 1
    },
    logo: {
        height: 150,
        width: 150,
        resizeMode: 'cover',
    },
    text: {
        fontSize: 28,
        marginBottom: 10,
        color: colorTheme.appThemeTealColor,
    },
});