import React from "react";
import { StyleSheet, View } from "react-native";
import FormButton from "../components/FormButton";
import { useAuth } from "../navigation/AuthProvider";
import AppHeader from "../components/AppHeader";
import IconButton from "../components/IconButton";
import { colorTheme } from "../utils/ColorTheme";

const Settings = ({ navigation }: any) => {
    const { logout } = useAuth();

    const left = (
        <IconButton iconDictionary='antDesign' iconType="left" onPress={() => navigation.goBack()} />
    )

    return (
        <View style={styles.mainContainer}>
            <AppHeader
                title={"Settings"}
                titleColor={colorTheme.white}
                left={left}
                backgroundColor={colorTheme.appSkyBlueBackgroundColor}
            />
            <View style={[styles.container, { flex: 1 }]}>
                <FormButton
                    buttonTitle="Create Group"
                    onPress={() => navigation.navigate("CreateGroup")}
                />
            </View>
            <View style={styles.container}>
                <FormButton
                    buttonTitle="Logout"
                    onPress={() => logout()}
                />
            </View>
        </View>
    );
}

export default Settings;

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colorTheme.white,
    },
    container: {
        padding: 10,
    },
    text: {
        fontSize: 28,
        marginBottom: 10,
        color: colorTheme.appThemeTealColor,
    },
});