import React, { useEffect, useState } from "react";
import { FlatList } from "react-native";
import { colorTheme } from "../utils/ColorTheme";
import { Container } from '../styles/HomeStyle'
import AppHeader from "../components/AppHeader";
import IconButton from "../components/IconButton";
import ChatCard from "../components/ChatCard";
import database from '@react-native-firebase/database';

const Home = ({ navigation }: any) => {
    const [groupsArray, setGroupsArray] = useState<any>([]);

    useEffect(() => {
        getGroups();
    }, []);

    const getGroups = async () => {
        database()
            .ref('groupList/')
            .on('value', snapshot => {
                // console.log('snapshot.val() ', snapshot.val())
                if (snapshot.val() === null) {
                    setGroupsArray([]);
                } else {
                    setGroupsArray(Object.values(snapshot.val()))
                }
            })
    }

    const right = (
        <IconButton iconDictionary='antDesign' iconType="setting" onPress={() => navigation.navigate("Settings")} />
    );

    return (
        <Container>
            <AppHeader
                title={"Home"}
                titleColor={colorTheme.white}
                right={right}
                backgroundColor={colorTheme.appThemeTealColor}
            />
            <FlatList
                data={groupsArray}
                style={{}}
                keyExtractor={item => item.groupId}
                renderItem={({ item }) => (
                    <ChatCard
                        onPress={() => navigation.navigate("ChatScreen", { groupName: item.groupName, groupId: item.groupId })}
                        groupName={item.groupName}
                    />
                )}
            />
        </Container>
    );
}

export default Home;