import React, { useState } from 'react';
import { StyleSheet, Text, ScrollView, Image, View, TouchableOpacity } from 'react-native';
import FormButton from '../components/FormButton';
import FormInput from '../components/FormInput';
import { useAuth } from '../navigation/AuthProvider';
import Spacer from '../components/Spacer';
import { colorTheme } from '../utils/ColorTheme';

const Signup = ({ navigation }: any) => {
    const { register } = useAuth();
    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [confirmPassword, setConfirmPassword] = useState();

    const signUpTapped = () => {
        const details = {
            firstName,
            lastName,
            email,
            password
        }
        register(details);
    }

    return (
        <View style={styles.mainContainer}>
            <ScrollView contentContainerStyle={styles.container}>
                <Image
                    source={require('../../assets/logo.png')}
                    style={styles.logo}
                />
                <Text style={styles.text}>Group Chat App</Text>

                <FormInput
                    labelValue={firstName}
                    onChangeText={(text: any) => setFirstName(text)}
                    placeholder="First Name"
                    iconType="user"
                    keyboardType="default"
                />

                <FormInput
                    labelValue={lastName}
                    onChangeText={(text: any) => setLastName(text)}
                    placeholder="Last Name"
                    iconType="user"
                    keyboardType="default"
                />

                <FormInput
                    labelValue={email}
                    onChangeText={(text: any) => setEmail(text)}
                    placeholder="Email"
                    iconType="mail"
                    keyboardType="email-address"
                    autoCapitalize="none"
                    autoCorrect={false}
                />

                <FormInput
                    labelValue={password}
                    onChangeText={(text: any) => setPassword(text)}
                    placeholder="Password"
                    iconType="lock"
                    secureTextEntry={true}
                />

                <FormInput
                    labelValue={confirmPassword}
                    onChangeText={(text: any) => setConfirmPassword(text)}
                    placeholder="Password"
                    iconType="lock"
                    secureTextEntry={true}
                />

                <Spacer height={50} />

                <FormButton
                    buttonTitle="Sign Up"
                    onPress={() => signUpTapped()}
                />

                <TouchableOpacity
                    style={styles.signup}
                    onPress={() => navigation.navigate('Login')}>
                    <Text style={styles.navButtonText}>
                        Click here to Sign In
                    </Text>
                </TouchableOpacity>


            </ScrollView>
        </View>
    );
}

export default Signup;

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colorTheme.white,
    },
    container: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: 20,

    },
    logo: {
        height: 150,
        width: 150,
        resizeMode: 'cover',
    },
    text: {
        fontSize: 28,
        marginBottom: 10,
        color: colorTheme.appThemeTealColor,
    },
    signup: {
        marginVertical: 35,
    },
    navButtonText: {
        fontSize: 18,
        fontWeight: '500',
        color: colorTheme.appThemeTealColor,
    },
});