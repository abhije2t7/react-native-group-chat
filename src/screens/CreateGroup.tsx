import React, { useState, useEffect } from "react";
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import FormButton from "../components/FormButton";
import AppHeader from "../components/AppHeader";
import IconButton from "../components/IconButton";
import { colorTheme } from "../utils/ColorTheme";
import FormInput from "../components/FormInput";
import ModalComponent from "../components/Modal";
import { WindowHeight } from "../utils/Dimensions";
import Spacer from "../components/Spacer";
import database from '@react-native-firebase/database';
import uuid from 'react-native-uuid';
import moment from "moment";

const CreateGroup = ({ navigation }: any) => {
    const [groupName, setGroupName] = useState('');
    const [isUserModal, setIsUserModal] = useState(false);
    const [selectedUsers, setSelectedUsers] = useState<any>([]);
    const [userArray, setUserArray] = useState<any>([]);

    useEffect(() => {
        getUsers();
    }, []);

    const getUsers = async () => {
        database()
            .ref('users/')
            .once('value')
            .then((snapshot) => {
                setUserArray(Object.values(snapshot.val()))
            })
    }

    const left = (
        <IconButton iconDictionary='antDesign' iconType="left" onPress={() => navigation.goBack()} />
    )

    const userModal = () => {
        return (
            <ModalComponent
                testID="userModal"
                modalStyle={styles.modal}
                isVisible={isUserModal}
                setIsVisible={() => setIsUserModal(false)}

            >
                <View style={styles.mainModalView}>
                    <View style={styles.closeButtonView}>
                        <IconButton iconDictionary='antDesign' iconType="check" onPress={() => setIsUserModal(false)} color={colorTheme.black} marginTop={1} />
                        <IconButton iconDictionary='antDesign' iconType="close" onPress={() => modalCloseTapped()} color={colorTheme.black} marginTop={1} />
                    </View>
                    <ScrollView
                        style={{
                            marginTop: 33,
                            marginHorizontal: 16,
                        }}>
                        <View style={styles.container}>
                            {userArray.map((data: any, index: number) => (
                                <Users
                                    key={index}
                                    item={data}
                                    id={data.userId.toString()}
                                    selected={isSelected(data)}
                                />
                            ))}
                        </View>

                    </ScrollView>
                </View>

            </ModalComponent>
        )
    }

    const modalCloseTapped = () => {
        setIsUserModal(false);
        setSelectedUsers([]);
    }

    const handlePress = (item: any) => {
        if (selectedUsers.includes(item)) {
            const newUserList = selectedUsers.filter((user: any) => user !== item);
            setSelectedUsers(newUserList);
        } else {
            setSelectedUsers((prev: any) => [...prev, item]);
        }

    }

    const isSelected = (id: any) => selectedUsers.includes(id);

    const Users = ({ item, id, selected, key }: any) => {
        return (
            <TouchableOpacity key={key} style={styles.userStyle} onPress={() => handlePress(item)}>
                <Text
                    style={styles.singleUserStyle}>
                    {item.firstName} {item.lastName}
                </Text>
                {selected && <View style={styles.overlay} />}
            </TouchableOpacity>
        );
    };

    const createGroupTapped = async () => {
        let id = uuid.v4();
        let data = {
            groupId: id,
            createdAt: moment().format(),
            groupName: groupName,
            users: selectedUsers
        }
        database().ref(`/groupList/${id}`)
            .set(data)
            .then(() => {
                console.log('Group created successfully');
                navigation.navigate('Settings');
            })
    }

    return (
        <View style={styles.mainContainer}>
            {userModal()}
            <AppHeader
                title={"Create Group"}
                titleColor={colorTheme.white}
                left={left}
                backgroundColor={colorTheme.appOrangeBackgroundColor}
            />
            <View style={[styles.container, { flex: 1 }]}>
                <FormInput
                    labelValue={groupName}
                    onChangeText={(text: any) => setGroupName(text)}
                    placeholder="Group Name"
                    iconType="team"
                />

                {selectedUsers.length > 0 &&
                    <View>
                        <Text style={styles.text}>Added User(s):</Text>
                        {selectedUsers.map((data: any, index: number) => (
                            <View key={index} style={styles.addedUserStyle}>
                                <Text
                                    style={styles.singleUserStyle}>
                                    {'\u2B24'}  {data.firstName} {data.lastName}
                                </Text>
                            </View>
                        ))}
                    </View>
                }
                <FormButton
                    buttonTitle={selectedUsers.length > 0 ? "Add more/remove user(s)" : "Add users"}
                    onPress={() => setIsUserModal(true)}
                />
                <Spacer flex={1} />
                <FormButton
                    buttonTitle="Create Group"
                    onPress={() => createGroupTapped()}
                />

            </View>
        </View>
    );
}

export default CreateGroup;

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: colorTheme.white,
    },
    container: {
        padding: 10,
    },
    text: {
        fontSize: 18,
        marginBottom: 10,
        color: colorTheme.black,
    },
    modal: {
        justifyContent: 'flex-end',
        margin: 0
    },
    mainModalView: {
        backgroundColor: colorTheme.white,
        borderTopRightRadius: 30,
        borderTopLeftRadius: 30,
        height: WindowHeight / 2,
    },
    closeButtonView: {
        padding: 20,
        flexDirection: 'row',
        justifyContent: 'space-between',
        borderBottomWidth: 0.5,
        borderColor: colorTheme.grayTextColor
    },
    userStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 2,
        padding: 10
    },
    singleUserStyle: {
        fontSize: 20,
        color: colorTheme.black,
    },
    overlay: {
        position: 'absolute',
        width: '100%',
        height: '120%',
        backgroundColor: 'rgba(0,0,0,0.2)',
        borderRadius: 10
    },
    addedUserStyle: {
        flexDirection: 'row',
        marginVertical: 2,
        padding: 10
    },

});