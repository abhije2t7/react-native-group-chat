import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

type IconButtonProps = {
    onPress?: () => void;
    iconType: string;
    color?: string;
    marginTop?: number;
    iconDictionary: string;
    disabled?: boolean;
}

const IconButton = ({
    onPress,
    iconType,
    color,
    marginTop,
    iconDictionary,
    disabled
}: IconButtonProps) => {

    const renderMessage = (item: string) => {
        switch (item) {
            case 'fontAwesome':
                return <FontAwesome name={iconType} size={25} color={color || "#FFF"} />
            case 'antDesign':
                return <AntDesign name={iconType} size={25} color={color || "#FFF"} />
            default:
                break;
        }
    }

    return (
        <TouchableOpacity
            style={{ marginTop: marginTop || 30 }}
            onPress={onPress}
            disabled={disabled}
        >
            {renderMessage(iconDictionary)}
        </TouchableOpacity>
    )
}

export default IconButton;