import React from 'react';
import { StyleSheet, View } from 'react-native';

const defaultSize = 16;

const Spacer = (props: any) => {

    return (
        <View style={{ height: props.height || defaultSize, width: props.width || defaultSize, flex: props.flex }}></View>
    )
}

export default Spacer;