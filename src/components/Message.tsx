import React from 'react';
import { View, Text, StyleSheet, Pressable, TouchableOpacity } from 'react-native';
import { colorTheme } from '../utils/ColorTheme';
import MessageDeliveryTime from './MessageDeliveryTime';
import database from '@react-native-firebase/database';
import { useAuth } from '../navigation/AuthProvider';

const Message = (props: any) => {
    const { sender, item, groupId } = props;
    const { setMessageLiked } = useAuth();

    const liketapped = () => {
        database()
            .ref(`/messages/${groupId}/${item.id}`)
            .update(item.isLiked ? {
                isLiked: false,
            } : {
                isLiked: true,
            })
            .then(() => {
                console.log('Message like toggled.');
                setMessageLiked(false);
            });
    }

    return (
        <Pressable>
            <View
                style={[styles.TriangleShapeCSS,
                sender ?
                    styles.right
                    :
                    [styles.left]
                ]}
            />
            <View
                style={[styles.messageBox, {
                    alignSelf: sender ? 'flex-end' : 'flex-start',
                    backgroundColor: sender ? colorTheme.appThemeTealColor : colorTheme.white
                }]}
            >
                <Text style={{ paddingLeft: 5, color: sender ? colorTheme.white : colorTheme.black, fontSize: 12.5 }}>
                    {item.message}
                </Text>

                <MessageDeliveryTime
                    sender={sender}
                    item={item}
                />
            </View>
            <TouchableOpacity
                style={[styles.LikeButton,
                sender ?
                    styles.rightLikeButton
                    :
                    [styles.leftLikeButton]
                ]}
                onPress={() => liketapped()}
            >
                {item.isLiked ? <Text style={styles.LikeButtonText}>Unlike</Text> : <Text style={styles.LikeButtonText}>Like</Text>}
            </TouchableOpacity>
        </Pressable>
    );
};

const styles = StyleSheet.create({
    messageBox: {
        alignSelf: 'flex-end',
        marginHorizontal: 10,
        minWidth: 80,
        maxWidth: '80%',
        paddingHorizontal: 10,
        marginVertical: 5,
        paddingTop: 5,
        borderRadius: 8
    },
    timeText: {
        fontFamily: 'AveriaSerifLibre-Light',
        fontSize: 10
    },
    dayview: {
        alignSelf: 'center',
        height: 30,
        width: 100,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 30,
        marginTop: 10
    },
    iconView: {
        width: 42,
        height: 42,
        borderRadius: 21,
        alignItems: 'center',
        justifyContent: 'center',
    },
    TriangleShapeCSS: {
        position: 'absolute',
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 15,
        borderRightWidth: 5,
        borderBottomWidth: 20,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
    },
    left: {
        borderBottomColor: colorTheme.white,
        left: 2,
        bottom: 32,
        transform: [{ rotate: '0deg' }]
    },
    right: {
        borderBottomColor: colorTheme.appThemeTealColor,
        right: 2,
        bottom: 30,
        transform: [{ rotate: '103deg' }]
    },
    LikeButton: {
        borderColor: colorTheme.appSkyBlueBackgroundColor,
        padding: 2,
        borderRadius: 5,
        borderWidth: 1,
        backgroundColor: colorTheme.white
    },
    leftLikeButton: {
        left: 15,
        bottom: 3,
        alignSelf: 'flex-start',
    },
    rightLikeButton: {
        right: 15,
        bottom: 3,
        alignSelf: 'flex-end'
    },
    LikeButtonText: {
        color: colorTheme.appSkyBlueBackgroundColor
    },
});

export default Message;