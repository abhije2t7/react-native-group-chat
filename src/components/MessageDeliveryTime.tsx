import moment from 'moment';
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { colorTheme } from '../utils/ColorTheme';

const MessageDeliveryTime = (props: any) => {
    const { sender, item } = props;
    return (
        <View
            style={[styles.mainView, {
                justifyContent: 'flex-end',
            }]}
        >
            <Text style={{
                fontSize: 7,
                color: sender ? colorTheme.white : colorTheme.grayTextColor
            }}>
                {moment(item.sentTime).format('LLL')}
            </Text>

        </View>
    );
};

const styles = StyleSheet.create({
    mainView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 2
    }
});

export default MessageDeliveryTime;