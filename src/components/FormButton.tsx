import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { colorTheme } from '../utils/ColorTheme';
import { WindowHeight } from '../utils/Dimensions';

const FormButton = ({ buttonTitle, ...props }: any) => {
    return (
        <TouchableOpacity style={styles.buttonContainer} {...props}>
            <Text style={styles.buttonText}>{buttonTitle}</Text>
        </TouchableOpacity>
    );
}

export default FormButton;


const styles = StyleSheet.create({
    buttonContainer: {
        marginTop: 10,
        width: '100%',
        height: WindowHeight / 15,
        backgroundColor: colorTheme.appTealBackgroundColor,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
    },
    buttonText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: colorTheme.white,
    },
});