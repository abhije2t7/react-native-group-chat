import React from 'react';
import { View, Text, StatusBar } from 'react-native';
import { styled } from '../styles/Theme';

type AppHeaderProps = {
    title?: React.ReactNode;
    titleColor?: string;
    left?: React.ReactNode;
    right?: React.ReactNode;
    backgroundColor?: string;
    isStatusBarSpacer?: boolean;
}

const Container = styled(View) <{ backgroundColor?: string }>`
    background-color: ${(props) => props.backgroundColor};
    flex-direction: row;
    align-items: center;
    justify-content: center;
    height: 90px;
    z-index: 1;
    padding-top: 30px;
`;

const LeftContainer = styled(View)`
    position: absolute;
    left: 15px;
    flex-direction: row;
    z-index: 1;
`;

const RightContainer = styled(View)`
    position: absolute;
    right: 15px;
    flex-direction: row;
    z-index: 1;
`;

const Title = styled(Text) <{ color?: string, size?: number }>`
    margin-horizontal: 85px;
    text-align: center;
    color: ${(props) => props.color || '#000'};
    font-size:  25px;
`;

const StatusBarSpacer = styled(View) <{ color?: string }>`
    height: 30px;
    background-color: ${({ color = 'transparent' }) => color};
`;

const AppHeader = ({
    title,
    titleColor,
    left,
    right,
    backgroundColor,
    isStatusBarSpacer = true,
}: AppHeaderProps) => {

    return (
        <Container backgroundColor={backgroundColor}>
            <StatusBar
                backgroundColor={backgroundColor}
                barStyle="light-content"
                translucent
                animated
            />
            <LeftContainer>{left}</LeftContainer>
            <Title color={titleColor}>{title}</Title>
            <RightContainer>{right}</RightContainer>
        </Container>
    )
}

export default AppHeader;