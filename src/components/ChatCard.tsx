import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import { colorTheme } from '../utils/ColorTheme';
import { WindowHeight } from '../utils/Dimensions';

type ChatCardProps = {
    onPress?: () => void;
    groupName?: string;
}

const ChatCard = ({
    onPress,
    groupName
}: ChatCardProps) => {
    return (
        <TouchableOpacity style={styles.buttonContainer} onPress={onPress}>
            <View style={styles.imageWrapper}>
                <Image style={styles.image} source={require('../../assets/groupImage.jpg')} />
            </View>
            <Text style={styles.buttonText}>{groupName}</Text>
        </TouchableOpacity>
    );
}

export default ChatCard;


const styles = StyleSheet.create({
    buttonContainer: {
        margin: 10,
        width: '95%',
        height: WindowHeight / 10,
        backgroundColor: colorTheme.white,
        padding: 10,
        alignItems: 'center',
        borderRadius: 10,
        elevation: 5,
        flexDirection: 'row'
    },
    buttonText: {
        fontSize: 16,
        fontWeight: 'bold',
        color: colorTheme.black,
        marginLeft: 10
    },
    image: {
        width: 50,
        height: 50,
        borderRadius: 25,
        borderColor: colorTheme.lightBlueColor,
        borderWidth: 1
    },
    imageWrapper: {
        paddingTop: 15,
        paddingBottom: 15,
    }
});