import React from 'react';
import Modal from 'react-native-modal';

type Props = {
    testID: string;
    isVisible?: boolean;
    setIsVisible?: () => void;
    children?: React.ReactNode;
    modalStyle?: any;
};

const ModalComponent = ({
    testID,
    isVisible,
    setIsVisible,
    children,
    modalStyle,
}: Props) => {
    return (
        <Modal
            testID={testID}
            isVisible={isVisible}
            style={modalStyle}
            onBackButtonPress={setIsVisible}
            onBackdropPress={setIsVisible}
            backdropOpacity={1}
        >
            {children}
        </Modal>
    );
};

export default ModalComponent;