import React, { useEffect, useState } from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Providers from './navigation';
import SplashScreen from 'react-native-splash-screen'


const AppStack = createNativeStackNavigator();

const App = () => {


  useEffect(() => {
    setTimeout(() => SplashScreen.hide(), 500);
  });

  return (
    <Providers />
  );
}

export default App;