export const colorTheme = {

    appBackgroundColor: "#181D21",
    appThemeTealColor: "#4AD5C7",
    white: "#FFFFFF",
    placeholderTextColor: "#6C757D",
    black: "#000000",
    appSkyBlueBackgroundColor: "#00BCD4",
    appOrangeBackgroundColor: "#FBB624",
    appTealBackgroundColor: "#4AD5C7",
    grayTextColor: "#98A3B0",
    errorColor: "#FF0000",
    lightBlueColor: "#94A0FD",
};
